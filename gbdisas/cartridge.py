"""
A complete Nintendo Game Boy cartridge.
"""


from logging import getLogger

from gbdisas.header import Header
from gbdisas.hexdump import to_hexdump
from gbdisas.instructions import load_instructions


class Cartridge:

    def __init__(self, raw: bytearray):
        self.raw = raw
        self.header = Header(raw)
        self.banks = []

        self._read_banks()

    def _read_banks(self):

        instructions = load_instructions()
        for idx in range(self.header.get_nb_rom_banks()):
            getLogger(__name__).debug("Loading bank %d", idx)

            addr = idx * 0x4000
            data = self.raw[addr:addr + 0x4000]
            self.banks.append(tuple(disas(data, instructions)))

    def dump_bank(self, bank: int):

        assert 0 <= bank < len(self.banks), f"Bank number {bank} does not exists."

        for addr, instr, data in self.banks[bank]:
            print(f"{addr:#06x}: {to_hexdump(data):16s}{instr.mnemonic}")


def disas(rom, instructions, offset=0):

    while rom:
        opcode = rom[0]
        instr = instructions[opcode]
        yield offset, instr, rom[0:instr.size]
        rom = rom[instr.size:]
        offset += instr.size
