"""
Helper function to display data in a hexdump style.
"""


def _line_split(data: bytearray, bytes_per_line: int):

    offset = 0
    while offset < len(data):
        yield data[offset:offset + bytes_per_line]
        offset += bytes_per_line


def to_hexdump(data: bytearray, bytes_per_line: int = 16, indent: int = 0) -> str:
    """
    Formats data in hexdump style.

    :param data: Data to format
    :param bytes_per_line: Number of bytes to display on one line.
    :param indent: Number of space chars to prefix each line with.
    :return: The formatted data as a string.
    """

    result = []
    indent_str = " " * indent

    for line in _line_split(data, bytes_per_line):
        fmt_str = indent_str + " ".join(["{:02x}",] * len(line))
        result.append(fmt_str.format(*list(line)))

    return "\n".join(result)
