"""
Command line entry point.
"""

from argparse import ArgumentParser, FileType
from logging import getLogger
from logging.config import dictConfig

from gbdisas.cartridge import Cartridge
from gbdisas.hexdump import to_hexdump


def setup_logging(n_level):

    levels = [
        "WARNING",
        "INFO",
        "DEBUG",
    ]

    level = levels[min(n_level, len(levels) - 1)]

    config = {
        "version": 1,
        "handlers": {
            "console": {
                "class": "logging.StreamHandler",
                "stream": "ext://sys.stdout",
                "level": level,
            }
        },
        "loggers": {
            __name__: {
                "level": level,
                "handlers": ["console",]
            }
        }
    }
    dictConfig(config)
    getLogger(__name__).debug("Verbosity set to: %s", level)


def main():
    parser = ArgumentParser()
    parser.add_argument("--verbose", "-v", action="count", default=0,
                        help="Increase verbosity. Can be used multiple times.")
    parser.add_argument("--header-only", action="store_true")
    parser.add_argument("--bank", "-b", type=int, action="append")
    parser.add_argument("rom", type=FileType("rb"))

    args = parser.parse_args()
    setup_logging(args.verbose)
    getLogger(__name__).info("Reading file: %s", args.rom.name)

    raw = bytearray(args.rom.read())
    cartridge = Cartridge(raw)

    print(cartridge.header)
    if args.header_only is True:
        exit(0)

    if args.bank is None:
        banks = range(cartridge.header.get_nb_rom_banks())
    else:
        banks = sorted(set(args.bank))

    for idx in banks:
        print(f"\nBank {idx}:\n")
        cartridge.dump_bank(idx)
