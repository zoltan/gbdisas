"""
Represent a cartridge header.
"""

from gbdisas.hexdump import to_hexdump


class Header:
    """
    GameBoy cartridge header.

    :param rom: The complete ROM to read the header from.
    """

    def __init__(self, rom: bytearray):

        assert rom[0x0143] & 0x80 == 0, "CGB format not supported"

        self.entrypoint = rom[0x0100:0x0103 + 1]
        self.nintendo_logo = rom[0x0104:0x0133 + 1]
        self.title = rom[0x0134:0x0143 + 1]
        self.new_licensee_code = rom[0x0144:0x0145 + 1]
        self.sgb_flag = rom[0x0146]
        self.cartridge_type = rom[0x0147]
        self.rom_size = rom[0x0148]
        self.ram_size = rom[0x0149]
        self.destination_code = rom[0x014A]
        self.old_licensee_code = rom[0x014B]
        self.game_version_number = rom[0x014C]
        self.header_checksum = rom[0x014D]
        self.global_checksum = rom[0x014E:0x014F + 1]

    def __str__(self) -> str:

        info = f"""Header:
    Entry point: .......... {to_hexdump(self.entrypoint)}
    Title: ................ {self.title.decode()}
    New licensee code: .... {self.describe_new_licensee_code()}
    SGB flag: ............. {self.sgb_flag:#04x}
    Cartridge type: ....... {self.describe_cartridge_type()}
    ROM size: ............. {self.describe_rom_size()}
    RAM size: ............. {self.describe_ram_size()}
    Destination code: ..... {self.describe_destination_code()}
    Old licensee code: .... {self.describe_old_licensee_code()}
    Game version number: .. {self.game_version_number:#04x}
    Header checksum: ...... {self.header_checksum:#04x}
    Global checksum: ...... {to_hexdump(self.global_checksum)}
    Nintendo logo:
{to_hexdump(self.nintendo_logo, indent=8)}
        """

        return info

    def describe_cartridge_type(self) -> str:

        mapping = {
            0x00: "ROM ONLY",
            0x01: "MBC1",
            0x02: "MBC1+RAM",
            0x03: "MBC1+RAM+BATTERY",
            0x05: "MBC2",
            0x06: "MBC2+BATTERY",
            0x08: "ROM+RAM",
            0x09: "ROM+RAM+BATTERY",
            0x0B: "MMM01",
            0x0C: "MMM01+RAM",
            0x0D: "MMM01+RAM+BATTERY",
            0x0F: "MBC3+TIMER+BATTERY",
            0x10: "MBC3+TIMER+RAM+BATTERY",
            0x11: "MBC3",
            0x12: "MBC3+RAM",
            0x13: "MBC3+RAM+BATTERY",
            0x19: "MBC5",
            0x1A: "MBC5+RAM",
            0x1B: "MBC5+RAM+BATTERY",
            0x1C: "MBC5+RUMBLE",
            0x1D: "MBC5+RUMBLE+RAM",
            0x1E: "MBC5+RUMBLE+RAM+BATTERY",
            0x20: "MBC6",
            0x22: "MBC7+SENSOR+RUMBLE+RAM+BATTERY",
            0xFC: "POCKET CAMERA",
            0xFD: "BANDAI TAMA5",
            0xFE: "HuC3",
            0xFF: "HuC1+RAM+BATTERY",
        }

        name = mapping.get(self.cartridge_type, "UNKNOWN")
        return f"{name} ({self.cartridge_type:#04x})"

    def describe_destination_code(self) -> str:

        mapping = {
            0: "JAPAN",
            1: "NON JAPAN",
        }
        name = mapping.get(self.destination_code, "UNKNOWN")
        return f"{name} ({self.destination_code:#04x})"

    def get_nb_rom_banks(self) -> int:
        mapping = {
            0x00: 2,
            0x01: 4,
            0x02: 8,
            0x03: 16,
            0x04: 32,
            0x05: 64,
            0x06: 128,
            0x07: 256,
            0x08: 512,
            0x52: 72,
            0x53: 80,
            0x54: 96,
        }
        return mapping[self.rom_size]

    def describe_rom_size(self) -> str:

        mapping = {
            0x00: "32 KByte - 2 banks (No ROM banking)",
            0x01: "64 KByte - 4 banks",
            0x02: "128 KByte - 8 banks",
            0x03: "256 KByte - 16 banks",
            0x04: "512 KByte - 32 banks",
            0x05: "1 MByte - 64 banks",
            0x06: "2 MByte - 128 banks",
            0x07: "4 MByte - 256 banks",
            0x08: "8 MByte - 512 banks",
            0x52: "1.1 MByte - 72 banks",
            0x53: "1.2 MByte - 80 banks",
            0x54: "1.5 MByte - 96 banks",
        }
        name = mapping.get(self.rom_size, "UNKNOWN")
        return f"{name} ({self.rom_size:#04x})"

    def describe_ram_size(self) -> str:

        mapping = {
            0x00: "0 - No RAM",
            0x01: "2 KB - Partial",
            0x02: "8 KB - 1 bank",
            0x03: "32 KB - 4 banks of 8 KB each",
            0x04: "128 KB - 16 banks of 8 KB each",
            0x05: "64 KB - 8 banks of 8 KB each",
        }
        name = mapping.get(self.ram_size, "UNKNOWN")
        return f"{name} ({self.ram_size:#04x})"

    def describe_old_licensee_code(self) -> str:

        # https://raw.githubusercontent.com/gb-archive/salvage/master/txt-files/gbrom.txt

        mapping = {
            0x00: "none",
            0x01: "nintendo",
            0x08: "capcom",
            0x09: "hot-b",
            0x0A: "jaleco",
            0x0B: "coconuts",
            0x0C: "elite systems",
            0x13: "electronic arts",
            0x18: "hudsonsoft",
            0x19: "itc entertainment",
            0x1A: "yanoman",
            0x1D: "clary",
            0x1F: "virgin",
            0x24: "pcm complete",
            0x25: "san-x",
            0x28: "kotobuki systems",
            0x29: "seta",
            0x30: "infogrames",
            0x31: "nintendo",
            0x32: "bandai",
            0x33: "see new code",
            0x34: "konami",
            0x35: "hector",
            0x38: "capcom",
            0x39: "banpresto",
            0x3C: "*entertainment i",
            0x3E: "gremlin",
            0x41: "ubi soft",
            0x42: "atlus",
            0x44: "malibu",
            0x46: "angel",
            0x47: "spectrum holoby",
            0x49: "irem",
            0x4A: "virgin",
            0x4D: "malibu",
            0x4F: "u.s. gold",
            0x50: "absolute",
            0x51: "acclaim",
            0x52: "activision",
            0x53: "american sammy",
            0x54: "gametek",
            0x55: "park place",
            0x56: "ljn",
            0x57: "matchbox",
            0x59: "milton bradley",
            0x5A: "mindscape",
            0x5B: "romstar",
            0x5C: "naxat soft",
            0x5D: "tradewest",
            0x60: "titus",
            0x61: "virgin",
            0x67: "ocean",
            0x69: "electronic arts",
            0x6E: "elite systems",
            0x6F: "electro brain",
            0x70: "infogrames",
            0x71: "interplay",
            0x72: "broderbund",
            0x73: "sculptered soft",
            0x75: "the sales curve",
            0x78: "t*hq",
            0x79: "accolade",
            0x7A: "triffix entertainment",
            0x7C: "microprose",
            0x7F: "kemco",
            0x80: "misawa entertainment",
            0x83: "lozc",
            0x86: "*tokuma shoten i",
            0x8B: "bullet-proof software",
            0x8C: "vic tokai",
            0x8E: "ape",
            0x8F: "i'max",
            0x91: "chun soft",
            0x92: "video system",
            0x93: "tsuburava",
            0x95: "varie",
            0x96: "yonezawa/s'pal",
            0x97: "kaneko",
            0x99: "arc",
            0x9A: "nihon bussan",
            0x9B: "tecmo",
            0x9C: "imagineer",
            0x9D: "banpresto",
            0x9F: "nova",
            0xA1: "hori electric",
            0xA2: "bandai",
            0xA4: "konami",
            0xA6: "kawada",
            0xA7: "takara",
            0xA9: "technos japan",
            0xAA: "broderbund",
            0xAC: "toei animation",
            0xAD: "toho",
            0xAF: "namco",
            0xB0: "acclaim",
            0xB1: "ascii or nexoft",
            0xB2: "bandai",
            0xB4: "enix",
            0xB6: "hal",
            0xB7: "snk",
            0xB9: "pony canyon",
            0xBA: "*culture brain o",
            0xBB: "sunsoft",
            0xBD: "sony imagesoft",
            0xBF: "sammy",
            0xC0: "taito",
            0xC2: "kemco",
            0xC3: "squaresoft",
            0xC4: "*tokuma shoten i",
            0xC5: "data east",
            0xC6: "tonkin house",
            0xC8: "koei",
            0xC9: "ufl",
            0xCA: "ultra",
            0xCB: "vap",
            0xCC: "use",
            0xCD: "meldac",
            0xCE: "*pony canyon or",
            0xCF: "angel",
            0xD0: "taito",
            0xD1: "sofel",
            0xD2: "quest",
            0xD3: "sigma enterprises",
            0xD4: "ask kodansha",
            0xD6: "naxat soft",
            0xD7: "copya systems",
            0xD9: "banpresto",
            0xDA: "tomy",
            0xDB: "ljn",
            0xDD: "ncs",
            0xDE: "human",
            0xDF: "altron",
            0xE0: "jaleco",
            0xE1: "towachiki",
            0xE2: "uutaka",
            0xE3: "varie",
            0xE5: "epoch",
            0xE7: "athena",
            0xE8: "asmik",
            0xE9: "natsume",
            0xEA: "king records",
            0xEB: "atlus",
            0xEC: "epic/sony records",
            0xEE: "igs",
            0xF0: "a wave",
            0xF3: "extreme entertainment",
            0xFF: "ljn",
        }

        name = mapping.get(self.old_licensee_code, "UNKNOWN")
        return f"{name} ({self.old_licensee_code:#04x})"

    def describe_new_licensee_code(self) -> str:

        # https://raw.githubusercontent.com/gb-archive/salvage/master/txt-files/gbrom.txt

        mapping = {
            "00": "none",
            "01": "nintendo",
            "08": "capcom",
            "13": "electronic arts",
            "18": "hudsonsoft",
            "19": "b-ai",
            "20": "kss",
            "22": "pow",
            "24": "pcm complete",
            "25": "san-x",
            "28": "kemco japan",
            "29": "seta",
            "30": "viacom",
            "31": "nintendo",
            "32": "bandia",
            "33": "ocean/acclaim",
            "34": "konami",
            "35": "hector",
            "37": "taito",
            "38": "hudson",
            "39": "banpresto",
            "41": "ubi soft",
            "42": "atlus",
            "44": "malibu",
            "46": "angel",
            "47": "pullet-proof",
            "49": "irem",
            "50": "absolute",
            "51": "acclaim",
            "52": "activision",
            "53": "american sammy",
            "54": "konami",
            "55": "hi tech entertainment",
            "56": "ljn",
            "57": "matchbox",
            "58": "mattel",
            "59": "milton bradley",
            "60": "titus",
            "61": "virgin",
            "64": "lucasarts",
            "67": "ocean",
            "69": "electronic arts",
            "70": "infogrames",
            "71": "interplay",
            "72": "broderbund",
            "73": "sculptured",
            "75": "sci",
            "78": "t*hq",
            "79": "accolade",
            "80": "misawa",
            "83": "lozc",
            "86": "tokuma shoten i*",
            "87": "tsukuda ori*",
            "91": "chun soft",
            "92": "video system",
            "93": "ocean/acclaim",
            "95": "varie",
            "96": "yonezawa/s'pal",
            "97": "kaneko",
            "99": "pack in soft",
        }

        code = chr(self.new_licensee_code[0]) + chr(self.new_licensee_code[1])
        name = mapping.get(code, "UNKNOWN")
        return f"{name} (\"{code}\")"
