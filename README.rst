GameBoy disassemble
===================

Quick start
-----------

.. code::

   $ poetry install
   $ poetry run gbdisas -v /path/to/rom.gb


Run ``gbdisas --help`` for a complete list of options.


Resources
---------

- https://www.devrs.com/gb/hardware.php
- https://gbdev.io/pandocs/
- https://bgb.bircd.org/pandocs.htm
- https://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html
